package counter

import (
	"testing"
)

var (
	counter = NewCounter()
)

func Test_Increment(t *testing.T) {
	for i := 0; i < 10; i++ {
		go concurIncr("count")
	}

	counterCopy = counter.GetCopy()
	if counterCopy["count"] != 10 {
		t.Error("Increment did not work as expected.")
	} else {
		t.Log("Increment test passed.")
	}
}

func Test_Reset(t *testing.T) {
	counter.Reset("count")

	counterCopy = counter.GetCopy()
	if counterCopy["count"] != 0 {
		t.Error("Reset did not work as expected.")
	} else {
		t.Log("Reset test passed.")
	}
}

func concurIncr(str string) {
	counter.Increment(str, 1)
}
