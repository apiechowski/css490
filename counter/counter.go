// Counter is a threadsafe counter.
// Counters can be incremented, reset, and dumped.
// Current version 1.0.
//
// Copyright 2015, Anna Piechowski (apiechow) <anna@apiechowski.com>
package counter

import (
	"sync"
)

type Counter struct {
	lock    sync.RWMutex
	counter map[string]int
}

func NewCounter() *Counter {
	c := new(Counter)
	c.counter = make(map[string]int)
	return c
}

func (c *Counter) Increment(key string, delta int) {
	c.lock.Lock()
	c.counter[key] = c.counter[key] + delta
	c.lock.Unlock()
}

func (c *Counter) Reset(key string) {
	c.lock.Lock()
	c.counter[key] = 0
	c.lock.Unlock()
}

func (c *Counter) GetCopy() map[string]int {
	counterCopy := make(map[string]int)
	c.lock.RLock()
	for key := range c.counter {
		counterCopy[key] = c.counter[key]
	}
	c.lock.RUnlock()
	return counterCopy
}
