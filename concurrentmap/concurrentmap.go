// Concurrent map package is a threadsafe map.
// Keys and values are both strings.
// Supports adding, deleting, getting values, and creating a copy of the map.
//
// Current version 1.0.
//
// Copyright 2015, Anna Piechowski (apiechow) <anna@apiechowski.com>
package concurrentmap

import (
	"sync"
)

// Concurrent map is a string map with its own RW lock.
type concurrentMap struct {
	lock      sync.RWMutex
	stringMap map[string]string
}

// Constructs and returns an empty ConcurrentMap.
func NewConcurrentMap() *concurrentMap {
	cm := new(concurrentMap)
	cm.stringMap = make(map[string]string)
	return cm
}

// Given a key, returns the value from the concurrent map.
// If the key is not in the map, return will be empty string.
func (cMap *concurrentMap) GetValue(key string) string {
	cMap.lock.RLock()
	value := cMap.stringMap[key]
	cMap.lock.RUnlock()
	return value
}

// Adds the given key/value pair to the map.
func (cMap *concurrentMap) Add(key string, value string) {
	cMap.lock.Lock()
	cMap.stringMap[key] = value
	cMap.lock.Unlock()
}

// Deletes the value from the map that is stored under the given key.
// If the key is not in the map, nothing will happen.
func (cMap *concurrentMap) Delete(key string) {
	cMap.lock.Lock()
	delete(cMap.stringMap, key)
	cMap.lock.Unlock()
}

// Makes a copy of the current map and returns it.
func (cMap *concurrentMap) GetMapCopy() (mapCopy map[string]string) {
	mapCopy = make(map[string]string)
	cMap.lock.RLock()
	for key, value := range cMap.stringMap {
		mapCopy[key] = value
	}
	cMap.lock.RUnlock()
	return mapCopy
}
