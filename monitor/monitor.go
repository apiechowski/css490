// Monitor is a server monitoring package.
// Collects stats at specified intervals by firing requests
// to the /monitor URI of all target URLs and reading the
// JSON encoded messages.  Displays all stats as a JSON-encoded
// string when finished.
//
// Current version 1.0
//
// Copyright 2015, Anna Piechowski (apiechow) <anna@apiechowski.com>
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	seelog "github.com/cihub/seelog"
)

type Sample struct {
	Time  time.Time
	Value int
}

const (
	// default log config file logger
	defLogConfig = "./etc/monitorLogConfig.xml"
)

// FLAGS
var targetsFlag = flag.String("targets", "", "comma-separated list of URLs to monitor")
var sampleIntervalFlag = flag.Int("sample-interval-sec", 10, "interval, in seconds, between sample requests")
var runtimeFlag = flag.Int("runtime-sec", 45, "time, in seconds, to run the monitor")
var logFlag = flag.String("log", defLogConfig, "config file for the logger")

// OTHER VARS
var (
	urlsToSample      []string
	interval, runtime time.Duration
	monitorClient     = new(http.Client)
	stats             = make(map[string][]Sample)
	logger, _         = seelog.LoggerFromConfigAsFile(defLogConfig) // default logger
)

// Runs the monitor program by firing requests to each URL's /monitor URI at specified intervals.
func runMonitor() {
	for {
		<-time.After(interval)
		logger.Debug("Firing getCounters")
		getCounters()
	}
}

// Gets all counters from the /monitor URI of all target URLs.
// Adds them to the map of stats.
func getCounters() {
	logger.Debug("In getCounters")
	for i := range urlsToSample {
		resp, err := monitorClient.Get(urlsToSample[i] + "/monitor")
		// if we get an error, just throw it out
		if err != nil {
			logger.Critical("No response from " + urlsToSample[i] + "/monitor")
		} else {
			// read the response body
			respData, err := ioutil.ReadAll(resp.Body)
			resp.Body.Close()
			if err != nil {
				logger.Critical("Error reading response from " + urlsToSample[i] + "/monitor")
			} else {
				// otherwise, unmarshal our JSON response and add it to our map of samples
				tempMap := make(map[string]int)
				json.Unmarshal(respData, &tempMap)
				for key, val := range tempMap {
					samp := new(Sample)
					samp.Time = time.UTC()
					samp.Value = val
					stats[key] = append(stats[key], *samp)
				}
			}
		}
	}
}

// Marshals the stats map into JSON and dumps it to the console.
func dump() {
	logger.Debug("In dump")
	json.Marshal(stats)
	fmt.Println(stats)
}

// Parses the flags and assigns them appropriately.
func init() {
	flag.Parse()

	interval = time.Duration(*sampleIntervalFlag) * time.Second
	runtime = time.Duration(*runtimeFlag) * time.Second

	// set up the logger with the passed config file
	logConfig := *logFlag
	logger, logConfigErr := seelog.LoggerFromConfigAsFile(logConfig)
	// if it didn't work, just use the default config file
	if logConfigErr != nil {
		logger, _ = seelog.LoggerFromConfigAsFile(defLogConfig)
	}
	seelog.ReplaceLogger(logger)
}

// Runs the monitor for a specified runtime.
func main() {

	// requires at least one target URL to run the monitor
	if *targetsFlag == "" {
		fmt.Println("Please specify at least one URL to monitor.")
		fmt.Println("Usage: monitor --targets <url1>,<url2>,...<urlN>")
	} else {
		urlsToSample = strings.Split(*targetsFlag, ",")
		for {
			defer dump()
			go runMonitor()
			time.Sleep(runtime)
			break
		}
	}
}
