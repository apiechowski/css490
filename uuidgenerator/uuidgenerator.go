// The UUIDGenerator package is a simple package that gets a UUID.
//
// Current version 1.0
//
// Copyright 2015, Anna Piechowski (apiechow) <anna@apiechowski.com>
package uuidgenerator

import (
	"bytes"
	"log"
	"os/exec"
)

// Gets a UUID by calling the uuidgen command (Unix only)
func GenUUID() (uuid string) {
	cmd := exec.Command("uuidgen")
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		log.Fatal(err)
	}

	uuid = out.String()
	//uuid = "100"

	return uuid
}
