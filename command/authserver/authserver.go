// Authserver package implements a simple authorization server that
// stores user cookie data. Data is routinely backed up to file.
// When the server is started, it will load user data from the backup
// file, if it is present.
//
// Current version 3.0
//
// Copyright 2015, Anna Piechowski (apiechow) <anna@apiechowski.com>
package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	cMap "bitbucket.org/apiechowski/concurrentmap"
	counter "bitbucket.org/apiechowski/counter"
	config "bitbucket.org/apiechowski/serverconfig"
	seelog "github.com/cihub/seelog"
)

// CONSTS
const (
	// persistent dumpfile path, so we always have one
	defaultDumpFilePath = "./dumpfile.dump"

	// default log config file
	defLogConfig = "./etc/authLogConfig.xml"
)

// VARS
var (
	authPortStr    string
	cookieMap      = cMap.NewConcurrentMap()                     // map for user cookies
	logger, _      = seelog.LoggerFromConfigAsFile(defLogConfig) // default logger
	monitorCounter = counter.NewCounter()                        // counter for the monitor
)

// Strings that will be used for the monitor counter
var (
	monCtrStrings = []string{
		"set-cookie",
		"get-cookie",
		"no-cookie",
		"200s",
		"400s",
		"404s",
	}
)

// Gets a usre cookie from the server.
func getHandler(w http.ResponseWriter, r *http.Request) {
	// increment monitor get count
	monitorCounter.Increment("get-cookie", 1)
	logger.Info(r)
	cookieKey := r.FormValue("cookie")
	username := cookieMap.GetValue(cookieKey)
	// if there was no name associated with the cookie, its a bad request
	if username == "" {
		// bad get - increment monitor count
		monitorCounter.Increment("no-cookie", 1)
		monitorCounter.Increment("400s", 1)
		logger.Error("Bad cookie GET request: ", r)
		w.WriteHeader(http.StatusBadRequest)
	} else {
		monitorCounter.Increment("200s", 1)
		w.Write([]byte(username))
	}
}

// Sets a user cookie on the server.
func setHandler(w http.ResponseWriter, r *http.Request) {
	// increment monitor set count
	monitorCounter.Increment("set-cookie", 1)
	// set always returns 200
	monitorCounter.Increment("200s", 1)

	logger.Info(r)
	cookieKey := r.FormValue("cookie")
	username := r.FormValue("name")

	// treat empty name as a delete request
	if username == "" {
		cookieMap.Delete(cookieKey)
	} else {
		cookieMap.Add(cookieKey, username)
	}
}

// Handles all requests to the URL path "/monitor"
// Simply dumps all monitor data into a JSON encoded dictionary
func monitorHandler(w http.ResponseWriter, r *http.Request) {
	logger.Info(r)

	// get a copy of the counter
	counterCopy := monitorCounter.GetCopy()
	// marshal it to json
	jsonMap, jsonErr := json.Marshal(counterCopy)
	// if this fails for some reason, log it (crit) and throw a 500
	if jsonErr != nil {
		logger.Critical("JSON marshaling for monitor failed")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	// otherwise, write the json to the response
	w.Write(jsonMap)
}

// Responds to all non get, set, or monitor requests with a 404.
func errHandler(w http.ResponseWriter, r *http.Request) {
	logger.Info(r)
	// increment the monitor counter
	monitorCounter.Increment("404s", 1)
	w.WriteHeader(http.StatusNotFound)
	fmt.Fprint(w, "404 Page Not Found")
}

// Reads in user cookie data from the given file and stores it on the server.
func initializeUserData(filename string) {

	tempMap, err := loadUserFile(filename)

	if err != nil {
		// remove the file, just in case it exists and is corrupted
		os.Remove(filename)
		// create a new (clean) version of the file
		_, fileErr := os.Create(filename)
		// if the create failed, we revert to our default dumpfile
		if fileErr != nil {
			os.Create(defaultDumpFilePath)
		}
	} else { // otherwise, loading was successful, we can use the data to init the cookie map
		for key, value := range tempMap {
			cookieMap.Add(key, value)
			fmt.Println("Key: " + key + " Value: " + value)
		}
	}
}

// Backs up the map of user data to the given file.
func backupMap(filename string) {
	// rename the old dumpfile to mark it as a backup
	os.Rename(filename, filename+".bak")

	// make a copy of the map
	mapCopy := cookieMap.GetMapCopy()

	// marshal the map into json
	jsonMap, jsonErr := json.Marshal(mapCopy)
	// if there is an error writing to json, revert to backup and bail
	if jsonErr != nil {
		os.Rename(filename+".bak", filename)
		logger.Debug("Auth server backup failed.")
		return
	}

	// make the new dumpfile
	dumpfile, fileErr := os.Create(filename)
	// if there is a file create error, revert to backup and bail
	if fileErr != nil {
		os.Rename(filename+".bak", filename)
		logger.Debug("Auth server backup failed.")
		return
	}

	// otherwise, write the json to the file
	dumpfile.Write(jsonMap)
	dumpfile.Close()

	// confirm file with map
	tempMap, loadErr := loadUserFile(filename)

	// if there was an error, revert to backup and bail
	if loadErr != nil {
		os.Rename(filename+".bak", filename)
		logger.Debug("Auth server backup failed.")
		return
	}

	copySuccessful := true
	// iterate over the new map, comparing it to the copy
	// if there was an error in backup, the maps won't be identical
	for key, _ := range tempMap {
		// if there was an error in copying, remove the new dumpfile and restore the backup
		if tempMap[key] != mapCopy[key] {
			copySuccessful = false
			os.Remove(filename)
			os.Rename(filename+".bak", filename)
			logger.Debug("Auth server backup failed.")
			break
		}
	}

	// if the copy was successful, delete the backup file
	if copySuccessful {
		os.Remove(filename + ".bak")
		logger.Debug("Auth server backup successful.")
	}
}

// Backs up the user data stored on the server to the given file.
// Backups occur regularly at the time interval (in seconds) provided by the parameter.
func incrementalBackup(seconds time.Duration, backupFilename string) {
	for {
		<-time.After(seconds)
		backupMap(backupFilename)
	}
}

// Loads user login data from a file and puts it into a map.
// Returns an error if the file fails to open, cannot be read, or cannot be loaded into a map.
func loadUserFile(filename string) (map[string]string, error) {
	// attempt to open given file
	tempfile, fileErr := os.Open(filename)
	// if open was unsuccessful, bail (return the error)
	if fileErr != nil {
		return nil, fileErr
	}
	// read the file and close it
	rawJson, fileReadErr := ioutil.ReadAll(tempfile)
	tempfile.Close()
	// if read was unsuccessful, bail (return the error)
	if fileReadErr != nil {
		return nil, fileReadErr
	}
	// make a map and marshal the json into it
	tempMap := make(map[string]string)
	jsonErr := json.Unmarshal(rawJson, &tempMap)
	// return the map and the json error
	return tempMap, jsonErr
}

// Sets up all vars and data necessary to run the server.
func setupServer() {
	// get the name of the dumpfile and load user data, if it has any
	dumpfilename := *config.DumpfileFlag
	initializeUserData(dumpfilename)

	// set up the logger with the passed config file
	logConfig := *config.LogFlag
	logger, logConfigErr := seelog.LoggerFromConfigAsFile(logConfig)
	// if it didn't work, just use the default config file
	if logConfigErr != nil {
		logger, _ = seelog.LoggerFromConfigAsFile(defLogConfig)
	}
	seelog.ReplaceLogger(logger)

	// get the time for incremental backups - if its greater than 0, kickoff the backup routine
	backupSeconds := *config.CheckpointIntervalFlag
	if backupSeconds > 0 {
		backupDuration := time.Duration(backupSeconds) * time.Second
		go incrementalBackup(backupDuration, dumpfilename)
	}

	// get the port
	authPortStr = ":" + *config.AuthPortFlag

	// ensure counters for the monitor are properly set at start
	setupMonitorCounter()
}

// Sets up the monitor counter for the authserver
func setupMonitorCounter() {
	for i := range monCtrStrings {
		monitorCounter.Reset(monCtrStrings[i])
	}
}

// Run that server!
func main() {
	// only run if the flag requesting the version was not set
	if !*config.VersionFlag {
		setupServer()

		http.HandleFunc("/get", getHandler)
		http.HandleFunc("/set", setHandler)
		http.HandleFunc("/monitor", monitorHandler)
		http.HandleFunc("/", errHandler)

		serverErr := http.ListenAndServe(authPortStr, nil)
		if serverErr != nil {
			log.Fatal("Auth Server Error: ", serverErr)
		}
	}
}
