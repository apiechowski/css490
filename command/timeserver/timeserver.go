// Timeserver package implements a simple web server that displays the current time.
// Time page can be personalized to display the user's name via a simple
// login form.
// Current version 5.1
//
// Copyright 2015, Anna Piechowski (apiechow) <anna@apiechowski.com>
//
// The seelog package  is  Copyright (c) 2012, Cloud Instruments Co., Ltd. <info@cin.io>
// All rights reserved.
package main

import (
	"encoding/json"
	"fmt"
	"html"
	"html/template"
	"log"
	"math/rand"
	"net/http"
	"sync"
	"time"

	cookies "bitbucket.org/apiechowski/cookies"
	counter "bitbucket.org/apiechowski/counter"
	config "bitbucket.org/apiechowski/serverconfig"
	seelog "github.com/cihub/seelog"
)

// CONSTS
const (

	// defines the format for the time strings
	localLayout = "3:04:05PM"
	utcLayout   = "15:04:05 UTC"

	// names for the cookie(s) used by the server
	usernameCookie = "username"

	// default log config file
	defLogConfig = "./etc/logConfig.xml"
)

// STRUCTS

// PersonTime stores a person's name and 2 time strings (local and UTC).
// Used to display a personalized time webpage.
type PersronTime struct {
	Name      string
	LocalTime string
	UTCTime   string
}

// concurrentCounter supplies a threadsafe integer counter
type ConcurrentCounter struct {
	lock  sync.Mutex
	count int
}

// VARS
var (
	port, templPath, logConfig, authHostname, authPort string
	maxInflightRequests, authTimeout                   int
	avgResponse, stdDev                                float64

	inflightRequests = new(ConcurrentCounter)                      // inflight request counter
	logger, _        = seelog.LoggerFromConfigAsFile(defLogConfig) // default logger
	monitorCounter   = counter.NewCounter()                        // counter for the monitor
)

// Maps of template file names
var (
	TemplateFiles = map[string]string{
		"index":  "/index.html",
		"login":  "/login.html",
		"logout": "/logout.html",
		"time":   "/time.html",
		"404":    "/404.html",
		"405":    "/405.html",
		"about":  "/about.html",
		"500":    "/500.html",
	}
)

// Map of templates - allows access to a particular temploate without knowledge of its "actual" name
var (
	ParsedTemplates = map[string]*template.Template{
		"index":  template.New("index"),
		"login":  template.New("login"),
		"logout": template.New("logout"),
		"time":   template.New("time"),
		"404":    template.New("404"),
		"405":    template.New("405"),
		"about":  template.New("about"),
		"500":    template.New("500"),
	}
)

// Strings that will be used for the monitor counter
var (
	monCtrStrings = []string{
		"login",
		"time-user",
		"time-anon",
		"200s",
		"404s",
	}
)

// FUNCTIONS

// Handles requests to the URL path "/time".
// Displays the current local time.
// If the user has a stored cookie, the user's name is
// also displayed.
func timeHandler(w http.ResponseWriter, r *http.Request) {

	// if we've been given a max inflight request number, check if we've hit the max concurrent requests
	if maxInflightRequests > 0 {
		logger.Debug("Checking inflight requests")
		inflightRequests.lock.Lock()
		currRequests := inflightRequests.count
		inflightRequests.lock.Unlock()
		// if we have hit the max requests, it's a 503
		if currRequests == maxInflightRequests {
			logger.Error("Hit max inflight requests ", r)
			w.WriteHeader(http.StatusInternalServerError)
			errHandler(w, r, http.StatusInternalServerError)
			return
		} else { // otherwise, increment current inflight requests
			logger.Debug("Incrementing inflight requests")
			inflightRequests.lock.Lock()
			inflightRequests.count++
			inflightRequests.lock.Unlock()
		}

	}

	// if we get here, we will return 200, so increment the monitor counter
	monitorCounter.Increment("200s", 1)

	// get our random response delay and sleep for it
	randDelay := rand.NormFloat64()*stdDev + avgResponse
	time.Sleep(time.Duration(randDelay) * time.Millisecond)

	logger.Info(r)
	pt := new(PersronTime)

	// find the username cookie
	myCookie, err := r.Cookie(usernameCookie)

	// If it was found, get the name
	if err == nil {
		pt.Name = cookies.GetCookieData(myCookie, authHostname, authPort, authTimeout)
		// if we got an empty string back, the auth server failed; log it and delete the cookie
		if pt.Name == "" {
			logger.Error("Auth server failed getting cookie: ", r)
			myCookie = cookies.ClearCookie(myCookie, authHostname, authPort, authTimeout)
			http.SetCookie(w, myCookie)
			monitorCounter.Increment("time-anon", 1) // increment requests fron anons
		} else {
			monitorCounter.Increment("time-user", 1) //increment requests from users
		}
	} else {
		monitorCounter.Increment("time-anon", 1) // increment requests from anons
	}

	t := time.Now()
	pt.LocalTime = t.Format(localLayout)
	t = t.UTC()
	pt.UTCTime = t.Format(utcLayout)

	ParsedTemplates["time"].Execute(w, pt)

	// decrement inflight requests
	if maxInflightRequests > 0 {
		logger.Debug("Decrementing inflight requests")
		inflightRequests.lock.Lock()
		inflightRequests.count--
		inflightRequests.lock.Unlock()
	}

}

// Handles all requests to the root of the server.
// Requests to the root or /index.html display a personalized greeting,
// if the user has a stored cookie.  Otherwise, the login form is displayed.
// All other requests result in 404 errors.
func homeHandler(w http.ResponseWriter, r *http.Request) {

	logger.Info(r)

	if r.URL.Path == "/" || r.URL.Path == "/index.html" {
		// if we get here, we will return 200, so increment the monitor counter
		monitorCounter.Increment("200s", 1)

		// Find the username cookie
		myCookie, err := r.Cookie(usernameCookie)
		// if there is no cookie, ask for a login
		if err != nil {
			ParsedTemplates["login"].Execute(w, nil)
		} else { // otherwise, get the name associated with the cookie
			username := cookies.GetCookieData(myCookie, authHostname, authPort, authTimeout)
			// if we got an empty string back, the auth server failed; log it and delete the cookie
			if username == "" {
				logger.Error("Auth server failed getting cookie: ", r)
				myCookie = cookies.ClearCookie(myCookie, authHostname, authPort, authTimeout)
				http.SetCookie(w, myCookie)
				// we will ask for a login, since the cookie was bad or auth server failed
				ParsedTemplates["login"].Execute(w, nil)
			} else {
				ParsedTemplates["index"].Execute(w, username)
			}
		}
	} else {
		w.WriteHeader(http.StatusNotFound)
		errHandler(w, r, http.StatusNotFound)
		return
	}
}

// Handles all requests to the URL path "/login".
// Displays a simple login form that allows the user to enter
// their name. Stores a cookie for a non-empty entries.
func loginHandler(w http.ResponseWriter, r *http.Request) {
	// user can only access via POST - a GET request will return a 405
	if r.Method != "POST" {
		logger.Warn("Login access as GET ", r)
		w.WriteHeader(http.StatusMethodNotAllowed)
		errHandler(w, r, http.StatusMethodNotAllowed)
		return
	} else {
		// valid login request, increment the monitor counter
		monitorCounter.Increment("login", 1)
		logger.Info(r)

		username := html.EscapeString(r.FormValue("name"))

		// make a new uuid cookie for the user and set it
		myCookie := cookies.MakeCookie(authHostname, authPort, usernameCookie, username, authTimeout)
		// if there was an error contacting the server (no cookie), just throw a 500
		if myCookie == nil {
			logger.Error("Auth server failed ", r)
			w.WriteHeader(http.StatusInternalServerError)
			errHandler(w, r, http.StatusInternalServerError)
			return
		}

		// if we get here, we will return 200, so increment the monitor counter
		monitorCounter.Increment("200s", 1)

		// otherwise, we're free to set the cookie
		http.SetCookie(w, myCookie)
		// redirect to home (302)
		http.Redirect(w, r, "/", http.StatusFound)
	}
}

// Handles all requests to the URL path "/logout".
// Logouts delete the user's cookie, then return the user
// to the root (home).
func logoutHandler(w http.ResponseWriter, r *http.Request) {

	logger.Info(r)

	// Find the username cookie
	myCookie, err := r.Cookie(usernameCookie)

	// If a cookie was found delete it
	if err == nil {
		myCookie = cookies.ClearCookie(myCookie, authHostname, authPort, authTimeout)
		// if there was an error contacting the server (no cookie), just throw a 500
		if myCookie == nil {
			logger.Error("Auth server failed ", r)
			w.WriteHeader(http.StatusInternalServerError)
			errHandler(w, r, http.StatusInternalServerError)
			return
		}
		http.SetCookie(w, myCookie)
	}

	// if we get here, we will return 200, so increment the monitor counter
	monitorCounter.Increment("200s", 1)

	ParsedTemplates["logout"].Execute(w, nil)
}

// Handles all requests to the URL path "/about"
func aboutHandler(w http.ResponseWriter, r *http.Request) {
	// if we get here, we will return 200, so increment the monitor counter
	monitorCounter.Increment("200s", 1)
	logger.Info(r)
	ParsedTemplates["about"].Execute(w, nil)
}

// Handles all requests to the URL path "/monitor"
// Simply dumps all monitor data into a JSON encoded dictionary
func monitorHandler(w http.ResponseWriter, r *http.Request) {
	logger.Info(r)

	// get a copy of the counter
	counterCopy := monitorCounter.GetCopy()
	// marshal it to json
	jsonMap, jsonErr := json.Marshal(counterCopy)
	// if this fails for some reason, log it (crit) and throw a 500
	if jsonErr != nil {
		logger.Critical("JSON marshaling for monitor failed")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	// otherwise, write the json to the response
	w.Write(jsonMap)
}

// Handles all erroneous requests to the server.
// Handles 405 and 500 explictily; treats all others as 404.
func errHandler(w http.ResponseWriter, r *http.Request, status int) {
	if status == http.StatusMethodNotAllowed {
		ParsedTemplates["405"].Execute(w, nil)
	} else if status == http.StatusInternalServerError {
		ParsedTemplates["500"].Execute(w, nil)
	} else {
		monitorCounter.Increment("404s", 1) // increment monitor count of 404s
		ParsedTemplates["404"].Execute(w, nil)
	}
}

// Sets up all vars and data necessary to run the server.
func setupServer() {
	port = ":" + *config.PortFlag
	templPath = *config.TemplateFlag
	logConfig = *config.LogFlag

	// auth server info
	authHostname = *config.AuthHostFlag
	authPort = *config.AuthPortFlag

	// configure the logger
	logger, logConfigErr := seelog.LoggerFromConfigAsFile(logConfig)
	// if it didn't work, just use the default config file
	if logConfigErr != nil {
		logger, _ = seelog.LoggerFromConfigAsFile(defLogConfig)
	}
	seelog.ReplaceLogger(logger)

	// set up max inflight restriction
	maxInflightRequests = *config.MaxInflightFlag

	// setup random response delay vars
	avgResponse = *config.AvgRespFlag
	stdDev = *config.StdDevFlag

	// set up auth timeout
	authTimeout = *config.AuthTimeoutFlag

	// add given filepath to front of all templates, try to parse them
	// bail if there is a parsing error
	for key, value := range TemplateFiles {
		TemplateFiles[key] = templPath + value
		// parse the header, footer, and menu with each template
		_, templateErr := ParsedTemplates[key].ParseFiles(templPath+"/menu.html", templPath+"/header.html", TemplateFiles[key], templPath+"/footer.html")
		if templateErr != nil {
			log.Fatal("Template Parse Error: ", templateErr)
		}
	}

	// ensure counters for the monitor are properly set up at start
	setupMonitorCounter()
}

// Sets up the monitor counter for the timeserver
func setupMonitorCounter() {
	for i := range monCtrStrings {
		monitorCounter.Reset(monCtrStrings[i])
	}
}

// MAIN

// Instantiate and run the server.
func main() {
	// if only the version was requested, we print that and do nothing else
	if *config.VersionFlag {
		fmt.Println("Timeserver v5.1")
	} else {

		setupServer()

		http.HandleFunc("/", homeHandler)
		http.HandleFunc("/about", aboutHandler)
		http.HandleFunc("/time", timeHandler)
		http.HandleFunc("/login", loginHandler)
		http.HandleFunc("/logout", logoutHandler)
		http.HandleFunc("/monitor", monitorHandler)
		serverErr := http.ListenAndServe(port, nil)
		if serverErr != nil {
			log.Fatal("Server Error: ", serverErr)
		}
	}
}
