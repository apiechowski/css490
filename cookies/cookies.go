// The Cookies package handles basic http cookie creation, clearing,
// and data retrieval.
// Communicates with an auth server (through a client) to get and
// set cookie information.
//
// Current version 2.0
//
// Copyright 2015, Anna Piechowski (apiechow) <anna@apiechowski.com>
package cookies

import (
	"net/http"

	authclient "bitbucket.org/apiechowski/authclient"
	uuidgen "bitbucket.org/apiechowski/uuidgenerator"
)

// Make a cookie with the given name and data, using the given auth server (host and port).
// Timeout parameter limits the time that the function will wait for the auth server.
// Returns the cookie if it was successfully made, otherwise nil.
func MakeCookie(authHost string, authPort string, cookieName string, cookiedata string, timeout int) *http.Cookie {

	cookie := new(http.Cookie)

	cookie.Name = cookieName
	cookie.Value = uuidgen.GenUUID()

	status := authclient.Set(authHost, authPort, cookie.Value, cookiedata, timeout)

	// if the set was unsuccessful, we won't return a cookie (nil instead)
	if status != http.StatusOK {
		return nil
	}

	return cookie
}

// Gets the data represented by the given cookie, using the given auth server (host and port).
// Timeout parameter limits the time that the function will wait for the auth server.
// Returns the data that the cookie represents.
func GetCookieData(cookie *http.Cookie, authHost string, authPort string, timeout int) string {

	data := authclient.Get(authHost, authPort, cookie.Value, timeout)
	return data
}

// Clears the given cookie and deletes any data the cookie stores on the given auth server (host and port).
// Timeout parameter limits the time that the function will wait for the auth server.
// Returns the cleared cookie.
func ClearCookie(cookie *http.Cookie, authHost string, authPort string, timeout int) *http.Cookie {

	// Delete cookie, don't care about status - empty string signals delete to auth client
	authclient.Set(authHost, authPort, cookie.Value, "", timeout)

	// Delete actual cookie data
	cookie.Value = ""
	cookie.MaxAge = -1

	// return cleared cookie
	return cookie
}
