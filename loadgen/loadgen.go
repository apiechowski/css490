// Load generator simulates load on a server by issuing bursts of
// concurrent requests to a given url.
// Used to test server capacity.
// Current version 1.0.
//
// Copyright 2015, Anna Piechowski (apiechow) <anna@apiechowski.com>
package main

import (
	"flag"
	"fmt"
	"net/http"
	"strconv"
	"time"

	ctr "bitbucket.org/apiechowski/counter"
	seelog "github.com/cihub/seelog"
)

const (
	// default log config file
	defLogConfig = "./etc/loadgenLogConfig.xml"
)

var (
	rate         int
	burst        int
	runtime      int
	url          string
	logConfig    string
	tickerPeriod time.Duration

	counter = ctr.NewCounter()
	client  = new(http.Client)

	counterNames = []string{
		"Total",
		"100s",
		"200s",
		"300s",
		"400s",
		"500s",
		"Errors",
	}

	rateFlag    = flag.Int("rate", 100, "average rate of requests per second")
	burstFlag   = flag.Int("burst", 10, "number of concurrent requests to issue")
	timeoutFlag = flag.Int("timeout-ms", 1000, "max time to wait for a response (in milliseconds)")
	runtimeFlag = flag.Int("runtime", 10, "number of seconds to process")
	urlFlag     = flag.String("url", "", "url to sample")
	logFlag     = flag.String("log", defLogConfig, "log config file")

	logger, _ = seelog.LoggerFromConfigAsFile(defLogConfig)
)

// Simulates load on the server.
// Kicks off a ticker based on the average request rate and fires off a burst of requests on each tick.
func load() {
	logger.Debug("Starting load.")
	ch := time.Tick(tickerPeriod)

	for _ = range ch {
		for i := 0; i < burst; i++ {
			logger.Debug("Firing request ", i)
			go request()
		}
	}
}

// Dumps the counters to the console.
func dump() {

	counterCopy := counter.GetCopy()
	for index := range counterNames {
		fmt.Print(counterNames[index] + ": ")
		fmt.Println(counterCopy[counterNames[index]])
	}
}

// Sends a request to the server/URL that the load generator is testing.
// Counts all requests, errors, and response codes.
func request() {
	counter.Increment("Total", 1)
	resp, err := client.Get(url)
	if err != nil {
		logger.Info("Request timed out.")
		counter.Increment("Errors", 1)
	} else {
		// add the response code to the counter
		// string is the century of the resp code + "s" (100s, 200s, etc.)
		logger.Info("Request returned status ", resp.StatusCode)
		codeStr := strconv.Itoa(resp.StatusCode-(resp.StatusCode%100)) + "s"
		counter.Increment(codeStr, 1)
	}
}

// Sets up the counter with all necessary counts.
func setupCounter() {
	for index := range counterNames {
		counter.Reset(counterNames[index])
	}
}

// Sets up the logger.
func setupLogger() {

	// configure the logger
	logger, logConfigErr := seelog.LoggerFromConfigAsFile(logConfig)
	// if it didn't work, just use the default config file
	if logConfigErr != nil {
		logger, _ = seelog.LoggerFromConfigAsFile(defLogConfig)
	}
	seelog.ReplaceLogger(logger)
}

// Parses all flags and assigns them to the correct vars.
func init() {
	flag.Parse()

	rate = *rateFlag
	burst = *burstFlag
	runtime = *runtimeFlag
	url = *urlFlag
	logConfig = *logFlag

	client.Timeout = time.Duration(*timeoutFlag) * time.Millisecond
	tickerPeriod = time.Duration(burst*1000000/rate) * time.Microsecond

	setupCounter()
	setupLogger()

}

// Run the load generator.
func main() {
	// if no URL was given to test, we don't run the load generator
	if url == "" {
		fmt.Println("Usage loadgen --url='http://myURL'")
	} else {
		for {
			defer dump()
			go load()
			// runtime converted to milliseconds with a 0.1 second offset (accounts for processing delays)
			time.Sleep(time.Duration((runtime*1000)+100) * time.Millisecond)
			break
		}
	}
}
