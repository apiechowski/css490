# README #

This is a basic web server in Go that returns the current time to the user.  Allows for a personalized time page if the user "logs in" with their name.

Contains the time server, an auth server, a monitor, and a load generator to simulate heavy use.

### Set Up ###

Install Go on your machine: http://golang.org/

From your GOROOT: go get bitbucket.org/apiechowski/css490

Everything you need will be installed in the src folder of your GOROOT.

### Note ###

This program makes use of the uuidgen command at will currently only work on Linux machines.