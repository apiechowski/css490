// The ServerConfig package gets all configuration data for Timeserver and Authserver.
// Config data is retrieved from command line flags.
//
// Current version 1.0.
//
// Copyright 2015, Anna Piechowski (apiechow) <anna@apiechowski.com>
package serverconfig

import (
	"flag"
)

// Default file paths
var defLogConfig = "./etc/logConfig.xml"
var defTemplPath = "./templates"
var defDumpPath = "./dump/authserver.dump"

// Command line flags (all publicly accessible)
var VersionFlag = flag.Bool("V", false, "current version of timeserver")
var PortFlag = flag.String("port", "8080", "specify a port for the timeserver")
var TemplateFlag = flag.String("templates", defTemplPath, "specify a filepath to the templates")
var LogFlag = flag.String("log", defLogConfig, "specify config file for logging")
var MaxInflightFlag = flag.Int("max-inflight", 0, "specify number of maximum concurrent requests allowed for the timeserver")

var AuthPortFlag = flag.String("authport", "9090", "specify a port for the auth server")
var AuthHostFlag = flag.String("authhost", "localhost", "specify the hostname for the auth server")
var AuthTimeoutFlag = flag.Int("authtimeout-ms", 10000, "specify a timeout for requests to the auth server (in ms)")

var AvgRespFlag = flag.Float64("avg-response-ms", 500, "specify an average expected response time for the timeserver (in ms)")
var StdDevFlag = flag.Float64("deviation-ms", 250, "specigy a standard deviation for the timeserver's response time (in ms)")

var DumpfileFlag = flag.String("dumpfile", defDumpPath, "specify a filename for the authserver to dump data to")
var CheckpointIntervalFlag = flag.Int("checkpoint-interval", 30, "specify a time interval for authserver dumps (in sec)")

// Parse all the flags at runtime
func init() {
	flag.Parse()
}
