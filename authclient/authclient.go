// The Authclient package sets up a client for communicating with the auth server that stores user data.
// Used to get and set information for user cookies.
//
// Current version 1.0
//
// Copyright 2015, Anna Piechowski (apiechow) <anna@apiechowski.com>
package authclient

import (
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

var client = new(http.Client)

// Gets the information contained in the cookie specified by the parameter UUID from the auth server
// A return of an empty string means the auth server failed.
func Get(hostname string, port string, key string, timeout int) (name string) {

	client.Timeout = (time.Duration(timeout) * time.Millisecond)

	resp, err := client.Get("http://" + hostname + ":" + port + "/get?cookie=" + key)

	// if there was an error or we got a non-200 http status, send back an empty string
	if err != nil || resp.StatusCode != http.StatusOK {
		name = ""
	} else { // otherwise, read the response body to get the name
		respData, err := ioutil.ReadAll(resp.Body)
		resp.Body.Close()
		// if there was an error reading the response body, send back an empty string
		if err != nil {
			name = ""
		} else { // otherwise, send back the name written in the body
			name = string(respData)
		}
	}
	return name
}

// Sets the user information on the auth server with the given cookie and name parameters.
// If the name parameter is the empty string, the auth server will delete the user information.
func Set(hostname string, port string, cookie string, name string, timeout int) (status int) {

	client.Timeout = (time.Duration(timeout) * time.Millisecond)

	cookieVals := url.Values{}
	cookieVals.Set("cookie", cookie)
	cookieVals.Add("name", name)
	qString := cookieVals.Encode()

	resp, err := client.PostForm("http://"+hostname+":"+port+"/set?"+qString, cookieVals)

	status = http.StatusOK

	if err != nil || resp.StatusCode != http.StatusOK {
		status = http.StatusBadRequest
	}

	return status
}
